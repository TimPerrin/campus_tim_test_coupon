Rails.application.routes.draw do

  mount Admin::Engine, :at => '/admin'

  get 'newsletter', to: 'pages#newsletter', as: 'newsletter'
  get 'extras', to: 'pages#extras', as: 'extras'
  get 'dictionary', to: 'pages#dictionary', as: 'dictionary'
  get 'i_hume_you', to: 'pages#i_hume_you', as: 'i_hume_you'
  get 'test/:level/:number', to: 'pages#test', as: 'test'
  get 'contact', to: 'pages#contact', as: 'contact'
  post 'contact', :to => 'pages#send_contact'
  get 'shop', to: 'purchases#shop', as: 'shop'
  post 'result' => 'achievements#results'
  get 'achievements' => 'achievements#index'

  devise_for :users

  root 'dashboard#index'
  get 'dashboard' => 'dashboard#index'

  resources :chapters do
    resources :supports, only: [:show]

    resources :contents do
      resources :supports, only: [:show]
    end
  end

  resources :supports, except: :all do
    resources :achievements, only: [:create]
  end

  # Problème au niveau des routes avec index, show et create -> Workaround avec index qui accessible via history_path
  post 'purchases', :to => 'purchases#create'
  scope :users do
    get 'history', :to => 'purchases#index'
    resources :purchases, only: [:show]
  end



  resources :students

  resources :groups do
    resources :students
  end

  resources :organisations, only: [:show] do
    resources :managers, exept: [:index]
  end

  devise_scope :user do
    # Route for create a private user
    post 'private_registration', :to => 'users/registrations#private_registration'
  end

  match '/403', :to => 'pages#forbidden', :as => 'forbidden', :via => :all
  match '/404', :to => 'pages#not_found', :as => 'not_found', :via => :all
  match '/500', :to => 'pages#internal_error', :as => 'internal_error', :via => :all

end
