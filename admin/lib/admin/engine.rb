require 'wice_grid'

module Admin
  class Engine < ::Rails::Engine
    isolate_namespace Admin
  end
end
