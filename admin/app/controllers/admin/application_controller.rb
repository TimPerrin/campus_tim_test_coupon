module Admin
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception

	# Vérifie l'authentification de l'utilisateur
	before_filter :authenticate_user!, :admin_user

	# Chargement du layout principal
	layout :layout_by_resource

  private

  	def admin_user
  		if current_user.type != 'Administrator'
        	return redirect_to "/403"
  		end
  	end

	def layout_by_resource
		if user_signed_in? 
			'admin/application'
		else
			'login'
		end
	end  

  end
end
