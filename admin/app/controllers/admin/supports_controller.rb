module Admin
	class SupportsController < ApplicationController

		def show
			@supportable = find_supportable
			@support = Support.find(params[:id])
		end

		def new
			@supportable = find_supportable
			@support = Support.new
			@support_types = SupportType.all
			@support_categories = SupportCategory.all
		end

		def edit
			@supportable = find_supportable
			@support = Support.find(params[:id])
			@support_types = SupportType.all
			@support_categories = SupportCategory.all
		end

		def create

			@supportable = find_supportable

			# TODO : Voir si autre solution possible
			additional_params = { number: Support.next_number_for_content(@supportable), supportable_id: @supportable.id, supportable_type: @supportable.class.name }

			@support = Support.new(additional_params.merge(support_params))

			@supportable.supports << @support

			if @support.save
				flash[:success] = 'New support added successfully'
				redirect_to polymorphic_path([@supportable, @support])
			else
				@support_types = SupportType.all
				@support_categories = SupportCategory.all
				flash[:error] = 'Validation error'
				render 'new'
			end
		end

		def update
			@supportable = find_supportable
			@support = Support.find(params[:id])

			if @support.update(support_params)
				flash[:success] = 'Support updated successfully'
				redirect_to polymorphic_path([@supportable, @support])
			else
				@support_types = SupportType.all
				@support_categories = SupportCategory.all
				flash[:error] = 'Validation error'
			  	render 'edit'
			end

		end

		def destroy
			@support = Support.find(params[:id])
			FileUtils.rm_r("public"+@support.root_path) if Dir.exists?("public"+@support.root_path())
			@support.destroy
			redirect_to :back
		end

		def change_order

			@support = Support.find(params[:id])

			@support.change_order(params[:order].split(","))

			respond_to do |format|
			  format.all { render :nothing => true, :status => 200 }
			end
		end

	private

		def find_supportable

			result = nil

			params.each do |name, value|
				if name =~ /(.+)_id$/
					result = $1.classify.constantize.find(value)
				end
			end
			return result
		end

		def support_params
	      	params.require(:support).permit(:name, :support_type_id, :support_category_id, :level, :file)
	    end
	end
end