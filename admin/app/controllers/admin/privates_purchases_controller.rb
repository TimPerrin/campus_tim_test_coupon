module Admin
  class PrivatesPurchasesController < ApplicationController
    def show
      @purchase = Purchase.find(params[:id])
      @private = Private.find(params[:private_id])
      @chapters_grid = initialize_grid(@purchase.chapters)
    end

    def new
      @private = Private.find(params[:private_id])
      @purchase = Purchase.new
      @chapter = Chapter.all
    end

    def create
      @private = Private.find(params[:private_id])
      @purchase = Purchase.new(private_purchase_params)

      @private.purchases << @purchase

      if @purchase.save
        flash[:success] = 'New purchase added successfully'
        redirect_to private_path(@private)
      else
        flash[:error] = 'Validation error'
        render 'new'
      end
    end

    def edit
      @private = Private.find(params[:private_id])
      @purchase = Purchase.find(params[:id])
      @chapter = Chapter.all
    end

    def update
      @purchase = Purchase.find(params[:id])

      if @purchase.update(private_purchase_params)
        flash[:success] = 'Pruchase updated successfully'
        redirect_to private_privates_purchase_path(params[:private_id], @purchase)
      else
        flash[:error] = 'Validation error'
        render 'edit'
      end
    end

    private

    def private_purchase_params
      params.require(:purchase).permit(:price, :from, :to, chapter_ids: [])
    end
  end
end