module Admin
  class OrganisationsPurchasesController < ApplicationController

    def show
      @purchase = Purchase.find(params[:id])
      @organisation = Organisation.find(params[:organisation_id])
      @chapters_grid = initialize_grid(@purchase.chapters)
    end

    def new
      @organisation = Organisation.find(params[:organisation_id])
      @purchase = Purchase.new
      @chapter = Chapter.all
    end

    def create
      @organisation = Organisation.find(params[:organisation_id])
      @purchase = Purchase.new(organisation_purchase_params)

      @organisation.purchases << @purchase

      if @purchase.save
        flash[:success] = 'New purchase added successfully'
        redirect_to organisation_path(@organisation)
      else
        flash[:error] = 'Validation error'
        render 'new'
      end
    end

    def edit
      @organisation = Organisation.find(params[:organisation_id])
      @purchase = Purchase.find(params[:id])
      @chapter = Chapter.all
    end

    def update
      @purchase = Purchase.find(params[:id])

      if @purchase.update(organisation_purchase_params)
        flash[:success] = 'Pruchase updated successfully'
        redirect_to organisation_organisations_purchase_path(params[:organisation_id], @purchase)
      else
        flash[:error] = 'Validation error'
        render 'edit'
      end
    end

    private

    def organisation_purchase_params
      params.require(:purchase).permit(:price, :from, :to, chapter_ids: [])
    end
  end
end