module Admin
	class SettingsController < ApplicationController
		
		def show
			@setting = Setting.find(1)
		end

		def edit
			@setting = Setting.find(1)
		end

		def update

			@setting = Setting.find(1)

			if @setting.update(setting_params)
				flash[:success] = 'Setting updated successfully'
				redirect_to setting_path(@setting)
			else
				flash[:error] = 'Validation error'
			  	render 'edit'
			end

		end

	private

		def setting_params
	      	params.require(:setting).permit(:description)
	    end
	end
end