module Admin
	class ChaptersController < ApplicationController
		def index
		  	@chapters_grid = initialize_grid(Chapter)
		end

		def show
			@chapter = Chapter.find(params[:id])

			@contents_grid = initialize_grid(@chapter.contents)
			@supports_grid = initialize_grid(@chapter.supports)
		end

		def new
			@chapter = Chapter.new
		end

		def edit
			@chapter = Chapter.find(params[:id])

			render 'edit'
		end

		def create

			# TODO : Voir si autre solution possible
			additional_params = { number: Chapter.next_number }
			@chapter = Chapter.new(additional_params.merge(chapter_params))

			if @chapter.save
				flash[:success] = 'New chapter added successfully'
				redirect_to chapter_path(@chapter)
			else
				flash[:error] = 'Validation error'
				render 'new'
			end
		end

		def update

			@chapter = Chapter.find(params[:id])

			if @chapter.update(chapter_params)
				flash[:success] = 'Chapter updated successfully'
				redirect_to chapter_path(@chapter)
			else
				flash[:error] = 'Validation error'
			  	render 'edit'
			end

		end

	private

		def chapter_params
	      	params.require(:chapter).permit(:name, :description, :release_date, :thumbnail)
	    end
	end
end