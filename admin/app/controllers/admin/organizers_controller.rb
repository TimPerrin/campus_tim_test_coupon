module Admin
	class OrganizersController < ApplicationController

		def new
			@organisation = Organisation.find(params[:organisation_id])
			@organizer = Organizer.new
		end

		def create

			@organisation = Organisation.find(params[:organisation_id])
			@organizer = Organizer.new(organizer_params)

			@organisation.users << @organizer

			if @organizer.save
				flash[:success] = 'New manager added successfully'
				redirect_to organisation_path(@organisation)
			else
				flash[:error] = 'Validation error'
				render 'new'
			end

		end

	private

		def organizer_params
	      	params.require(:organizer).permit(:first_name, :last_name, :email, :password, :password_confirmation)
	    end
	end
end