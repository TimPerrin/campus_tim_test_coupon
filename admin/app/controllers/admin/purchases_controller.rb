module Admin
  class PurchasesController < ApplicationController
    def index
      @purchases_grid = initialize_grid(Purchase)
    end
  end
end