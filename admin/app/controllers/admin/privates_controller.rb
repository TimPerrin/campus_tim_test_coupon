module Admin
  class PrivatesController < ApplicationController
    def index
      @privates_grid = initialize_grid(Private)
    end

    def show
      @private = Private.find(params[:id])
      @purchases_grid = initialize_grid(@private.purchases)
    end
  end
end