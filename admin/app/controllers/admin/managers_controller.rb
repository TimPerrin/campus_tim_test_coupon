module Admin
	class ManagersController < ApplicationController

		def new
			@organisation = Organisation.find(params[:organisation_id])
			@manager = Manager.new
		end

		def create

			@organisation = Organisation.find(params[:organisation_id])
			@manager = Manager.new(manager_params)

			@organisation.users << @manager

			if @manager.save
				flash[:success] = 'New manager added successfully'
				redirect_to organisation_path(@organisation)
			else
				flash[:error] = 'Validation error'
				render 'new'
			end

		end

	private

		def manager_params
	      	params.require(:manager).permit(:first_name, :last_name, :email, :password, :password_confirmation)
	    end
	end
end