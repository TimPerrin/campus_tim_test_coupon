module Admin
  class OrganisationsController < ApplicationController

    def index
      @organisations_grid = initialize_grid(Organisation)
    end

    def show
      @organisation = Organisation.find(params[:id])

      @nb_managers = @organisation.managers.count
      @nb_students = @organisation.students.count

      @managers_grid = initialize_grid(@organisation.users.where("type = 'Manager'"))
      @organizers_grid = initialize_grid(@organisation.users.where("type = 'Organizer'"))
      @purchases_grid = initialize_grid(@organisation.purchases)

    end

    def new
      @organisation = Organisation.new
    end

    def edit
      @organisation = Organisation.find(params[:id])
    end

    def create

      @organisation = Organisation.new(organisation_params)

      if @organisation.save
        flash[:success] = 'New organisation added successfully'
        redirect_to organisation_path(@organisation)
      else
        flash[:error] = 'Validation error'
        render 'new'
      end
    end

    def update

      @organisation = Organisation.find(params[:id])

      if @organisation.update(organisation_params)
        flash[:success] = 'Organisation updated successfully'
        redirect_to organisation_path(@organisation)
      else
        flash[:error] = 'Validation error'
        render 'edit'
      end

    end

    private

    def organisation_params
      params.require(:organisation).permit(:name, :nb_account_manager, :nb_account_student)
    end
  end
end