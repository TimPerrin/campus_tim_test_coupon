module Admin
	class ContentsController < ApplicationController
		def index
		  	@contents_grid = initialize_grid(Content)
		end

		def show
			@chapter = Chapter.find(params[:chapter_id])
			@content = Content.find(params[:id])
		end

		def new
			@chapter = Chapter.find(params[:chapter_id])
			@content = Content.new
			@content_types = ContentType.all
		end

		def edit
			@chapter = Chapter.find(params[:chapter_id])
			@content = Content.find(params[:id])
			@content_types = ContentType.all

			render 'edit'
		end

		def create

			@chapter = Chapter.find(params[:chapter_id])

			# TODO : Voir si autre solution possible
			additional_params = { number: Content.next_number(@chapter) }
			@content = Content.new(additional_params.merge(content_params))

			@chapter.contents << @content

			if @content.save
				flash[:success] = 'New content added successfully'
				redirect_to chapter_content_path(@chapter,@content)
			else
				@content_types = ContentType.all
				flash[:error] = 'Validation error'
				render 'new'
			end
		end

		def update

			@chapter = Chapter.find(params[:chapter_id])
			@content = Content.find(params[:id])

			if @content.update(content_params)
				flash[:success] = 'Content updated successfully'
				redirect_to chapter_content_path(@chapter,@content)
			else
				@content_types = ContentType.all
				flash[:error] = 'Validation error'
			  	render 'edit'
			end

		end

	private

		def content_params
	      	params.require(:content).permit(:name, :content_type_id, :thumbnail)
	    end
	end
end