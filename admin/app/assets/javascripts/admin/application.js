// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require wice_grid.js
//= require_tree .

$(document).on("ready page:update", function () {

    // --------------------------------------------------
    // Drag and Drop pour l'ordonnencement des supports
    // --------------------------------------------------
    var fixHelper = function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    };

    $(".sortable-support").sortable({
      revert: true,
      handle:".sort-btn",
      helper: fixHelper,
      update: function(event, ui) { 
        var array = new Array();

        $(ui.item.parent()).children().each(function(){
            array.push($(this).attr("data-id"));  
        });

        var link = $(this).attr("data-link");

        $.ajax({
          type: "GET",
          url: link,
          data: "order=" + array.toString() + "&id=" + ui.item.data("id")
        });

      }
    });

    // --------------------------------------------------
    // SELECT/UNSELECT ALL CHECKBOXES
    // --------------------------------------------------
    $('#selectAll').click(function() {
       if (this.checked) {
           $(':checkbox').each(function() {
               this.checked = true;                        
           });
       } else {
          $(':checkbox').each(function() {
               this.checked = false;                        
           });
       } 
    });


    // --------------------------------------------------
    // Rend clicable les ligne des tableaux 
    // --------------------------------------------------
    $('.wice-grid > tbody, .tr-clickable').on('click', 'tr', function(event) {
        if(event.target.nodeName != "A" && event.target.nodeName != "I") {
            window.document.location = $(this).attr("href");
        }    
    });


    // --------------------------------------------------
    // Menu Asider
    // --------------------------------------------------
    $('#side-menu').metisMenu();


	//Loads the correct sidebar on window load,
	//collapses the sidebar on window resize.
	// Sets the min-height of #page-wrapper to window size

    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent();

    while (true) {
        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }

});