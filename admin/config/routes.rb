Admin::Engine.routes.draw do

  root 'chapters#index'

  resources :settings

  resources :chapters do
    resources :supports, except: [:index]
    resources :contents
  end

  resources :contents do
    resources :supports, except: [:index]
  end

  resources :organisations do
    resources :managers, only: [:new, :create]
    resources :organizers, only: [:new, :create]
    resources :organisations_purchases, except: [:index, :destroy]
  end

  resources :privates do
    resources :privates_purchases, except: [:index, :destroy]
  end

  resources :purchases, only: [:index, :show]

  get "supports/change_order" => "supports#change_order", as: 'change_order_support'

end
