#!/bin/sh

bundle install

RAILS_ENV=production rake db:migrate

RAILS_ENV=production rake assets:precompile

service apache2 restart

