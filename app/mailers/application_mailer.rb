class ApplicationMailer < ActionMailer::Base
  default from: 'info@englishincontext.net'
  layout 'mailer'
end
