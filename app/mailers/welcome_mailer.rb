class WelcomeMailer < ApplicationMailer

  # layout 'welcome_mailer'

  def welcome_email(email)
    mail(to: email, subject: 'Let\'s go!')
  end
end
