/**
 * Created by afresco on 30.12.16.
 */

$(document).on("page:update", function () {

    //Handles on click for each items
    $(".buyable").on("click",function() {
        selectItem($(this));

        setPrice();
    });

    // Handles the select all button
    $("#select-all-item").on("click",function(){
        if($(".buyable").length != $(".selected").length){
            $(".buyable").each(function () {
                $(this).find("input").val($(this).data("chapter"));
                $(this).find(".image-content").addClass("selected");
            });
        }
        else{
            $(".buyable").each(function () {
                $(this).find("input").val("");
                $(this).find(".image-content").removeClass("selected");
            });
        }

        setPrice();
    });
});

function setPrice(){
    if($(".buyable").length == $(".selected").length){
        $total =  $(".selected").length * 5.0 * 0.9;
    }
    else{
        $total =  $(".selected").length * 5.0;
    }

    $(".shop-total").html($total.toFixed(2));
}

function selectItem(item) {
    if(item.find("input").val() == "") {
        item.find("input").val(item.data("chapter"));
        item.find(".image-content").addClass("selected");
    }
    else{
        item.find("input").val("");
        item.find(".image-content").removeClass("selected");
    }
}