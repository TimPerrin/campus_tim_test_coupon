$(document).on("page:update", function () {
    $(".content_row").hide();
    $(".chapter_row").on("click",function(){
       $(this).next().toggle();
    });

    $(".achievement-table .popover-btn").popover({ trigger: "hover" });
});
