require 'rubygems'
require 'zip'

class FileUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    # TODO : Voir comment éviter d'utiliser la condition pour le polymorphic (supportable_type)
    if model.supportable_type == "Chapter"
      supportable = Chapter.find(model.supportable_id)
      "SUPPORT/#{supportable.number}/#{model.number}"
    elsif model.supportable_type == "Content"
      supportable = Content.find(model.supportable_id)
      "SUPPORT/#{supportable.chapter.number}/#{supportable.number}/#{model.number}"
    end
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process scale: [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process resize_to_fit: [50, 50]
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_whitelist
    %w(zip)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  def filename
    "file_#{model.number}.#{file.extension}" if original_filename
  end

  version :specificFile do
    process :extract_file
  end

  def extract_file

    # TODO : Voir comment éviter d'utiliser la condition pour le polymorphic (supportable_type)
    if model.supportable_type == "Chapter"
      supportable = Chapter.find(model.supportable_id)
      destination = "public/SUPPORT/#{supportable.number}/#{model.number}/FILES/"
    elsif model.supportable_type == "Content"
      supportable = Content.find(model.supportable_id)
      destination = "public/SUPPORT/#{supportable.chapter.number}/#{supportable.number}/#{model.number}/FILES/"
    end

    FileUtils.rm_r(destination) if Dir.exists?(destination)

    Zip::File.open(current_path) do |zipfile|
      zipfile.each do |file|
        fpath = File.join(destination, file.name)
        FileUtils.mkdir_p(File.dirname(fpath))
        zipfile.extract(file, fpath)
      end
      File.delete(current_path)
    end
  end

end
