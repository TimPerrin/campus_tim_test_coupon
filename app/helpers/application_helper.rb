module ApplicationHelper
	def nav_link(link_text, link_path, controllers, actions = nil)

		class_name = ''

		controllers.each do |controller|
			if params[:controller] == controller
				if actions.present?
					actions.each do |action|
						if params[:action] == action
							class_name = 'active'
							break
						end
					end
				else
					class_name = 'active'
					break
				end
			end
		end

		# Crée la balise HTML correspondante
		content_tag(:li, :class => class_name) do
			link_to link_text, link_path
		end
	end
end
