class Group < ActiveRecord::Base

  has_many :students
  belongs_to :manager

  has_many :accesses

  has_many :contents, :through => :accesses, :source => :accessable, :source_type => "Content"
  has_many :chapters, :through => :accesses, :source => :accessable, :source_type => "Chapter"

  validates :name, :presence => true

end