class SupportType < ActiveRecord::Base

	has_many :supports
	has_many :packages

	validates :name, presence: true

end