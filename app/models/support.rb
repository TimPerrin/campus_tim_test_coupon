class Support < ActiveRecord::Base

  before_create :init_no
  after_destroy :reorder_no

  mount_uploader :file, FileUploader

  belongs_to :supportable, polymorphic: true
  belongs_to :support_type
  belongs_to :support_category
  has_many :achievements

  validates :name, :supportable_id, :support_type_id, presence: true

  default_scope { order('no') }

  scope :visible, lambda { |user|
    joins(:support_type).where('support_types.name <> "Print"') unless user.student?
  }

  validates :name, :supportable_id, :support_type_id, presence: true

  def self.available(user)
    supports = []
    Access.where("group_id = ?", user.group.id).each do |access|
      supports.push(access.accessable.supports)
    end
    supports
  end

  def score_by_user(user)
    if self.achievements.where(user: user).first.present?
      self.achievements.where(user: user).order(score: :desc).first.score
    else
      0
    end
  end

  def self.next_number_for_content(content)
    max = content.supports.maximum(:number)
    number ||= max ? max + 1 : 1
  end

  def self.next_number_for_chapter(chapter)
    max = chapter.supports.maximum(:number)
    number ||= max ? max + 1 : 1
  end

  def root_path(content_id = nil)
    @path = self.supportable.support_path + self.supportable.number.to_s + '/' + self.number.to_s + '/FILES'
  end

  def init_no
    self.no = Support.where(supportable: self.supportable, support_category: self.support_category).length + 1
  end

  def reorder_no
    Support.where(supportable: self.supportable, support_category: self.support_category).where("no > ?", self.no).each do |support|
      support.no -= 1
      support.save
    end
  end

  def change_order(list)
    Support.where(supportable: self.supportable, support_category: self.support_category).each do |support|
      support.no = list.index(support.id.to_s) + 1
      support.save
    end
  end

  def degree_achievement(user)
    achievement = self.achievements.where(:user_id => user.id).first

    if achievement.blank?
      return 'not_done'
    elsif achievement.score < 60
      return 'in-progress'
    else
      return 'done'
    end
  end

  def degree_achievement_side_menu(user)
    achievement = self.achievements.where(:user_id => user.id).first

    if achievement.blank?
      return ''
    elsif achievement.score < 60
      return '<i class="pull-right fa fa-fire" aria-hidden="true"></i>'
    else
      return '<i class="pull-right fa fa-check" aria-hidden="true"></i>'
    end
  end

end
