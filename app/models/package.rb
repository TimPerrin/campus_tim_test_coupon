class Package < ActiveRecord::Base

	belongs_to :content_type
	belongs_to :support_type

	validates :content_type, :support_type, presence: true

end