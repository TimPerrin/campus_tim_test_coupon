class Manager < User

  	has_many :groups

	validate :nb_max_account

  	def nb_max_account

		if self.organisation.managers.count + 1 > self.organisation.nb_account_manager
			errors.add(:nb_max_account, "no manager account available")
		end
  	end

end