class ContentType < ActiveRecord::Base

	has_many :contents
	has_many :packages

	validates :name, presence: true

end