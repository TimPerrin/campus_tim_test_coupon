class Content < ActiveRecord::Base

  mount_uploader :thumbnail, ThumbnailUploader

  belongs_to :content_type
  belongs_to :chapter
  has_many :supports, as: :supportable
  has_many :accesses, as: :accessable

  validates :name, :content_type, presence: true

  def self.next_number(chapter)
    max = chapter.contents.all.maximum(:number)
    number ||= max ? max + 1 : 1
  end

  def support_path
    '/SUPPORT/' + self.chapter.number.to_s + '/'
  end

  # Renvoi si l'utilisateur a effectué le test ou non
  def test(user)
    if user.type == 'Student' || user.type == 'Private'
      self.supports.where('support_type_id = ? and (level = ? or level = 0)', SupportType.find_by_name("Quiz").id, user.level).first
    else
      nil
    end
  end
end
