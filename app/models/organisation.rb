class Organisation < ActiveRecord::Base
  before_destroy :contain_user

  has_many :users
  has_many :managers
  has_many :students
  has_many :purchases, as: :purchasable

  validates :name, :nb_account_manager, :nb_account_student, :presence => true
  validates :nb_account_manager, :nb_account_student, numericality: {only_integer: true}

  # TODO : Prévoir validation dans le cas d'un update, pour la vérification du nombre 
  # nb_account_manager doit etre plus grand ou égal au total de manager présent dans l'organisation
  # Idem pour les étudiants

  def full_name
    self.name
  end

  # TODO: TO DELETE
  def active_purchase?
    self.purchases.where("purchases.to >= ?", Time.now).count > 0
  end

  def chapters
    Chapter.joins(subscriptions: :purchase)
        .where("purchases.purchasable_id = ? AND purchases.purchasable_type = 'Organisation' AND purchases.from <= ? AND purchases.to >= ?", self.id, DateTime.now, DateTime.now).distinct
  end

  private

  def contain_user
    self.users.empty?
  end

end