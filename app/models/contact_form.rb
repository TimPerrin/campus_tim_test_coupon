class ContactForm < MailForm::Base
  attribute :name
  attribute :email,      :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :message,    :validate => true

  # Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.
  def headers
    {
      :subject => "Contact Form English in Context: #{name}",
      :to => 'info@wavemind.ch ; info@englishincontext.net',
      :from => 'info@wavemind.ch'
    }
  end
end