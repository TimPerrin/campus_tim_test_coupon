class Student < User
  belongs_to :group

  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }
  validate :nb_max_account

  def chapters_achieved
    chapters = Chapter.joins(:supports)
        .joins(supports: :achievements)
        .where("user_id = ?",self.id)
        .where(:supports => {supportable_type: "Chapter"})
        .distinct

    contents = self.all_contents_achieved
    contents.each do |content|
      unless chapters.include?(content.chapter)
        chapters.push content.chapter
      end
    end
    chapters
  end

  def contents_achieved(chapter_id)
    Content.joins(:supports)
        .joins(supports: :achievements)
        .where("user_id = ?",self.id)
        .where("supportable_type = 'Content'")
        .where("chapter_id = ?",chapter_id)
        .distinct
  end

  def all_contents_achieved
    Content.joins(:supports)
        .joins(supports: :achievements)
        .where("user_id = ?",self.id)
        .where("supportable_type = 'Content'")
        .distinct
  end

  def nb_max_account
    if self.organisation.students.count + 1 > self.organisation.nb_account_student
      errors.add(:nb_max_account, "no student account available")
    end
  end

  def student?
    true
  end
end