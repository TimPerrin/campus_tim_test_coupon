class Purchase < ActiveRecord::Base
  
	belongs_to :purchasable, polymorphic: true
  	has_many :subscriptions
  	has_many :chapters, :through => :subscriptions

end