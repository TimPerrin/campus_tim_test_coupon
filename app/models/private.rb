class Private < User
  has_many :purchases, as: :purchasable

  def full_name
    self.first_name + " " + self.last_name
  end

  def chapters_achieved
    chapters = Chapter.joins(:supports)
                   .joins(supports: :achievements)
                   .where("user_id = ?",self.id)
                   .where(:supports => {supportable_type: "Chapter"})
                   .distinct

    contents = self.all_contents_achieved
    contents.each do |content|
      unless chapters.include?(content.chapter)
        chapters.push content.chapter
      end
    end
    chapters
  end

  def contents_achieved(chapter_id)
    Content.joins(:supports)
        .joins(supports: :achievements)
        .where("user_id = ?",self.id)
        .where("supportable_type = 'Content'")
        .where("chapter_id = ?", chapter_id)
        .distinct
  end

  def all_contents_achieved
    Content.joins(:supports)
        .joins(supports: :achievements)
        .where("user_id = ?",self.id)
        .where("supportable_type = 'Content'")
        .distinct
  end
end