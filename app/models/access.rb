class Access < ActiveRecord::Base

  belongs_to :group
  belongs_to :accessable, polymorphic: true

  # TODO : Voir pour faire une seule requete SQL (au lieu des trois ci-dessous) pour récupérer tout les chapitres et contenus dont le groupe à accès
  def self.all_access_chapters(group)
    group.manager.organisation.chapters.joins("LEFT JOIN contents ON chapters.id = contents.chapter_id").joins("LEFT JOIN accesses ON contents.id = accessable_id and accessable_type = 'Content' OR chapters.id = accessable_id and accessable_type = 'Chapter'").where("accesses.group_id = ?",group.id).distinct
  end

  def self.access_to_chapter?(group, chapter)
    Access.where("accessable_type = 'Chapter' AND accessable_id = ? AND group_id = ?",chapter.id,group.id).present?
  end

  def self.contents_access(group, chapter)
  	Content.joins("LEFT JOIN accesses ON contents.id = accessable_id and accessable_type = 'Content'").where("accesses.accessable_type = 'Content' AND accesses.group_id = ? AND contents.chapter_id = ?",group.id, chapter.id)
  end

  def self.supports_access(group, chapter)
    Support.joins("LEFT JOIN chapters ON chapters.id = supportable_id and supportable_type = 'Chapter'").joins("LEFT JOIN accesses ON chapters.id = accessable_id and accessable_type = 'Chapter'").where("accesses.accessable_type = 'Chapter' AND accesses.group_id = ? AND accesses.accessable_id = ?", group.id, chapter.id)
  end

end