class User < ActiveRecord::Base
	# Include default devise modules. Others available are:
	# :lockable, :timeoutable and :omniauthable
	devise :invitable, :database_authenticatable, :recoverable, :registerable, :rememberable, :trackable, :validatable, :confirmable

	belongs_to :organisation
	has_many :achievements, dependent: :destroy

	validates :first_name, :last_name, :presence => true

	enum level: {level_1: 1, level_2: 2}

	def full_name
		self.first_name + " " + self.last_name
	end

	def full_address
		txt = ""

		txt += self.address + ", " if self.address.present?
		txt += self.zip + ", " if self.zip.present?
		txt += self.city if self.city.present?

		return txt
	end

	def chapters
		Chapter.joins(subscriptions: :purchase)
			.where("purchases.purchasable_id = ? AND purchases.purchasable_type = 'User' AND purchases.from <= ? AND purchases.to >= ?", self.id, DateTime.now, DateTime.now).distinct
	end

	def active_purchase?
		if self.type == 'Organizer' || self.type == 'Manager' || self.type == 'Student'
			self.organisation.purchases.where("purchases.to >= ?", Time.now).count > 0
		elsif self.type == 'Private'
			self.purchases.where("purchases.to >= ?", Time.now).count > 0
		else
			true
		end
	end

	def remaining_time
		purchase = self.purchases.where('purchases.to > ?', Date.today).reverse.first
		((purchase.to - Time.now) / 1.days).ceil
	end

	def free_content?
		self.purchases.where('purchases.price = ? AND purchases.to >= ?', 0.0, Date.today).first
	end

	def other_purchase?
		self.purchases.where('purchases.to >= ? AND purchases.price != ?', Date.today, 0.0).count == 0
	end

	def student?
		false
	end

end