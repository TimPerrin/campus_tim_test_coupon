class Chapter < ActiveRecord::Base

	mount_uploader :thumbnail, ThumbnailUploader

	has_many :contents
	has_many :subscriptions
	has_many :supports, as: :supportable
	has_many :accesses, as: :accessable

	def self.next_number
		max = Chapter.all.maximum(:number)
		number ||= max ? max + 1 : 1
	end

	def self.get_all(user)
		if user.type == 'Student'
			chapters = Access.all_access_chapters(user.group)
		elsif user.type == 'Administrator'
			chapters = Chapter.all
		elsif user.type == 'Private'
			chapters = user.chapters
		else
			chapters = user.organisation.chapters
		end
		chapters.order(:number)
	end

	def self.get_contents(user, chapter)
		if user.type == 'Student'
			contents = Access.contents_access(user.group, chapter)
		else
			contents = chapter.contents
		end
	end

	def self.get_supports(user, chapter)
		if user.type == 'Student'
			tools = Access.supports_access(user.group, chapter)
		else
			tools = chapter.supports
		end
	end

	def support_path
		'/SUPPORT/'
	end

	def test(user)
		if user.type == 'Student' || user.type == 'Private'
			total = 0

			if self.contents.present?
				self.contents.each do |content|
					support = content.test(user)
					if support.present?
						total += support.score_by_user(user)
					end
				end
				total = total / self.contents.count
			else
				total
			end
		else
			total
		end
	end

	def degree_achievement(user)
		if user.type == 'Student' || user.type == 'Private'
			total = self.test(user).to_i

			if total == 0
				'not_done'
			elsif total < 60
				'in-progress'
			else
				'done'
			end
		else
			''
		end
	end

end
