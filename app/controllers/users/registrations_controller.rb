class Users::RegistrationsController < Devise::RegistrationsController

  def private_registration
    build_resource(registration_params)

    resource.type = 'Private'
    resource.level = 1

    if resource.save

      @purchase = Purchase.new(chapter_ids: Chapter.all.map {|c| c.id})
      @purchase.from = DateTime.now.to_date
      @purchase.to = DateTime.now.to_date + 15.days
      @purchase.price = 0

      @private = Private.find(resource)

      @private.purchases << @purchase

      WelcomeMailer.welcome_email(resource.email).deliver_later

      if @purchase.save
        flash[:success] = 'You have to confirm your email address before continuing.'
        redirect_to new_user_session_path
      else
        flash[:error] = 'Validation error'
        render 'devise/registrations/new'
      end

    else
      flash[:error] = 'Validation error'
      render 'devise/registrations/new'
    end
  end

  private

  def registration_params
    params.require(:user).permit(:first_name, :last_name, :address, :zip, :city, :email, :password, :password_confirmation)
  end

end