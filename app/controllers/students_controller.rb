class StudentsController < ApplicationController

  before_filter do |c|
    @group = Group.find(params[:group_id])
    add_breadcrumb "My groups", groups_path, :title => "Back to the Index"
    add_breadcrumb @group.name, group_path(@group), :title => "Back to the Index"
  end

  before_action :require_permission

  def show
    @student = Student.find(params[:id])
    @contents_grid = initialize_grid(Student.find(params[:id]).group.contents)

    add_breadcrumb @student.full_name, group_student_path(@group,@student), :title => "Back to the Index"
  end

  def new
    @student = Student.new
    add_breadcrumb "New student", new_group_student_path(@group), :title => "Back to the Index"
  end

  def create
    @group = Group.find(params[:group_id])
    @student = Student.new(student_params)
    @group.manager.organisation.students << @student
    @group.students << @student

    @student.valid?
    @student.errors.messages.except!(:password) #remove password from errors

    if (@student.errors.any?)
      flash[:error] = 'Validation error'
      render 'new'
    else
      @student.invite!
      flash[:success] = 'New student added successfully'
      redirect_to group_path(params[:group_id])
    end

  end

  def edit
    @student = Student.find(params[:id])
    @group = Group.find(params[:group_id])

    add_breadcrumb @student.full_name, group_student_path(@group,@student), :title => "Back to the Index"
    add_breadcrumb "Edit", edit_group_student_path(@group,@student), :title => "Back to the Index"
  end

  def update
    @student = Student.find(params[:id])
    @group = Group.find(params[:group_id])

    if @student.update(student_params)
      flash[:success] = 'Student updated successfully'
      redirect_to group_student_path(@group, @student)
    else
      flash[:error] = 'Validation error'
      render 'edit'
    end
  end

  def destroy

    @student = Student.find(params[:id])
    @group = Group.find(params[:group_id])

    if !@student.destroy
      flash[:error] = @student.errors[:base].first
    end

    respond_to do |format|
      format.html { redirect_to group_path(@group) }
      format.json { head :no_content }
    end
  end

  private

  def student_params
    params.require(:student).permit(:first_name, :last_name, :address, :zip, :city, :email, :description, :level, :password, :password_confirmation)
  end

  def require_permission
    # Possède le rôle
    if current_user.type != 'Manager'
      return redirect_to forbidden_path
    end

    # Possède l'étudiant
    if params[:id].present?
      @student = Student.find(params[:id])
      if @student.group.manager != current_user
        return redirect_to forbidden_path
      end
    end

    # Apartient a un groupe
    if params[:group_id].present?
      @group = Group.find(params[:group_id])
      if @group.manager != current_user
        return redirect_to forbidden_path
      end
    end
  end

end