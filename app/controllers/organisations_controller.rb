class OrganisationsController < ApplicationController

	before_action :require_permission

	def show
	  	@organisation = Organisation.find(params[:id])

	    @nb_managers = @organisation.managers.count
	    @nb_students = @organisation.students.count

	    @managers_grid = initialize_grid(@organisation.managers)
	    @purchases_grid = initialize_grid(@organisation.purchases)

	    add_breadcrumb @organisation.name, organisation_path(@organisation), :title => "Back to the Index"

	end

private

  def require_permission
    # Possède le rôle
    if current_user.type != 'Organizer'
      return redirect_to forbidden_path
    end

    # Appartient à l'organisation
    if params[:id].present?
      @organisation = Organisation.find(params[:id])
      if current_user.organisation != @organisation
        return redirect_to forbidden_path
      end
    end
  end

end