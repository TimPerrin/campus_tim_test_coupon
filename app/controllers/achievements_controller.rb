class AchievementsController < ApplicationController

  skip_before_action :verify_authenticity_token, only: [:results]
  skip_before_filter :authenticate_user!, only: [:results]

  def create
    #TODO Protect from achieving all support by forgging request

    # Tests if achievement already exists
    achievement = Achievement.where(:user_id => current_user.id).where(:support_id => params[:support_id]).first

    if achievement.nil? then

      #if not creates one
      achievement = Achievement.new(user_id: current_user.id, support_id: params[:support_id])
      achievement.score = 75

      if achievement.save then
        render status: :created, json: {message: 'Created!', achievement: achievement}
      else
        format.json { render json: achievement.errors, status: :unprocessable_entity }
      end
      # if exists return error page
    else
      render status: :not_acceptable, json: {message: 'Already exists', achievment: achievement}
    end
  end

  def index
    @user = User.find(current_user.id)
  end

  def results

    achievement = Achievement.where(user_id: params[:UID], support_id: params[:SID], ispring_token: params[:TKN]).first
    if achievement.present?

      achievement.score = params[:sp].to_f * 100  / params[:tp].to_f 

      if achievement.save
        render text: "OK"
      end
    else
      render text: ''
    end
  end

end
