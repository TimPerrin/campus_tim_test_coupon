class GroupsController < ApplicationController
  before_action :require_permission

  def index
    @groups_grid = initialize_grid(Group.where('manager_id = ?', current_user))
  end

  def show
    @group = Group.find(params[:id])
    @students_grid = initialize_grid(Student.where('group_id = ?', params[:id]))
    @chapters_grid = initialize_grid(Access.all_access_chapters(@group))

    add_breadcrumb @group.name, group_path(@group), :title => "Back to the Index"

  end

  def edit
    @group = Group.find(params[:id])
    @chapters = current_user.organisation.chapters

    add_breadcrumb @group.name, group_path(@group), :title => "Back to the Index"
    add_breadcrumb "Edit", edit_group_path(@group), :title => "Back to the Index"
  end

  def update
    @group = Group.find(params[:id])

    # Si aucun chapitre sélectionné, forcé le paramètre à vide (permet d'indiquer à ActiveRecord la modification)
    if params[:group][:chapter_ids].blank?
      params[:group][:chapter_ids] = [""]
    end

    # Si aucun contenu sélectionné, forcé le paramètre à vide (permet d'indiquer à ActiveRecord la modification)
    if params[:group][:content_ids].blank?
      params[:group][:content_ids] = [""]
    end

    if @group.update(group_params)
      flash[:success] = 'Content updated successfully'
      redirect_to group_path(@group)
    else
      @contents = Content.all
      @chapters = current_user.organisation.chapters
      flash[:error] = 'Validation error'
      render 'edit'
    end
  end

  def new
    @group = Group.new
    @chapters = current_user.organisation.chapters

    add_breadcrumb "New", new_group_path(@group), :title => "Back to the Index"
  end

  def create
    @group = Group.new(group_params)
    @group.manager_id = current_user.id

    if @group.save
      flash[:success] = 'New group added successfully'
      redirect_to group_path(@group)
    else
      @contents = Content.all
      @chapters = current_user.organisation.chapters
      flash[:error] = 'Validation error'
      render 'new'
    end
  end


private

  def group_params
    params.require(:group).permit(:name, :description, content_ids: [], chapter_ids: [])
  end

  def require_permission
    # Possède le rôle
    if current_user.type != 'Manager'
      return redirect_to forbidden_path
    end

    # Possède le groupe
    if params[:id].present?
      @group = Group.find(params[:id])
      if @group.manager != current_user
        return redirect_to forbidden_path
      end
    end
  end

end
