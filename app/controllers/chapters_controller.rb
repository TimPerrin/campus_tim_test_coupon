class ChaptersController < ApplicationController

  before_filter do |c|
    add_breadcrumb "My course", chapters_path, :title => "Back to the Index"
  end

  before_action :require_permission

  def show
    @chapter = Chapter.find(params[:id])
    @chapters = Chapter.get_all(current_user)
    @support_categories = SupportCategory.all
    @contents = Chapter.get_contents(current_user,@chapter)
    @supports = Chapter.get_supports(current_user, @chapter)

    add_breadcrumb @chapter.name, chapter_path(@chapter), :title => "Back to the Index"
  end

  private

  def require_permission

    # Possède le chapitre
    if params[:id].present?
      chapter = Chapter.find(params[:id])
      chapters = Chapter.get_all(current_user)
      if !chapters.include?(chapter)
        return redirect_to forbidden_path
      end
    end

  end

end