class PagesController < ApplicationController

  def newsletter
    add_breadcrumb "Newsletter", newsletter_path, :title => "Back to the Index"
  end

  def dictionary
    add_breadcrumb "Dictionary", dictionary_path, :title => "Back to the Index"
  end

  def extras
  end

  def i_hume_you
  end

  def test
    @title = 'Test ' + params['level'] + ' ' + params['number']
    @path = '/TESTS/' + params['level'] + '/' + params['number'] + '/'
  end

  def contact
    @contact = ContactForm.new
  end

  def send_contact
    @contact = ContactForm.new(contact_params)

    if @contact.valid?
      @contact.deliver
      flash[:success] = "Your message has been sent"
      redirect_to contact_path
    else
      flash[:error] = "Please complete correctly the form"
      render "pages/contact"
    end

  end

  def forbidden
    render :status => 403
  end

  def not_found
    render :status => 404
  end

  def internal_error
    render :status => 500
  end

  private

  def contact_params
    params.require(:contact_form).permit(:name, :email, :message)
  end

end