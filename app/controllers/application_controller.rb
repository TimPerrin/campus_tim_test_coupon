class ApplicationController < ActionController::Base
	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
	protect_from_forgery with: :exception

	# Vérifie l'authentification de l'utilisateur
	before_filter :authenticate_user!
	after_filter :cors_set_access_control_headers

	# Chargement du layout principal
	layout :layout_by_resource

	# Ajout des paramètres autorisé pour devise
	before_action :configure_permitted_parameters, if: :devise_controller?

	add_breadcrumb "Home", :root_path

	def cors_set_access_control_headers
		headers['Access-Control-Allow-Origin'] = '*'
		headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
		headers['Access-Control-Request-Method'] = '*'
		headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
	end

	def not_found
		respond_to do |format|
			format.html { redirect_to not_found_path }
			format.xml { head :not_found }
			format.any { head :not_found }
		end
	end

	def internal_error
		respond_to do |format|
			format.html { redirect_to internal_error_path }
			format.xml { head :not_found }
			format.any { head :not_found }
		end
	end

  protected

	  def configure_permitted_parameters
	    devise_parameter_sanitizer.permit(:sign_up, keys: [:last_name,:first_name])
	    devise_parameter_sanitizer.permit(:account_update, keys: [:last_name,:first_name,:address,:zip,:city, :level])
	  end

  private

	def layout_by_resource
		if user_signed_in? 
			'application'
		else
			'login'
		end
	end  
end
