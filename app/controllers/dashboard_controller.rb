class DashboardController < ApplicationController

  def index
    @chapters = Chapter.get_all(current_user)
    @setting = Setting.find(1)
  end

end