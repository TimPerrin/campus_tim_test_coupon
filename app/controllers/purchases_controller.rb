class PurchasesController < ApplicationController

	before_action :require_permission

	def index
		@purchases_grid = initialize_grid(Purchase.where(:purchasable_id => current_user.id))
	end

	def show
		@purchase = Purchase.find(params[:id])

		@chapters_grid = initialize_grid(@purchase.chapters)
	end

	def shop
		@chapters = Chapter.all
		if current_user.type == 'Private'
			@chapters_bought = current_user.chapters
		else
			@chapters_bought = current_user.organisation.chapters
		end

		@purchase = Purchase.new
	end

	def create

		# Amount in cents
		@amount = 6990

		customer = Stripe::Customer.create(
			:email => params[:stripeEmail],
			:source => params[:stripeToken]
		)

		charge = Stripe::Charge.create(
			:customer => customer.id,
			:amount => @amount,
			:description => 'All chapters',
			:currency => 'usd'
		)

		if charge.status == "succeeded"


			@purchase = Purchase.new(chapter_ids: Chapter.all.map {|c| c.id})
			@purchase.price = @amount / 100.0
			@purchase.stripe_id = charge.id
			@purchase.stripe_card = charge.source.brand
			@purchase.stripe_last4 = charge.source.last4

			if current_user.type == 'Organizer'
				@purchase.from = Date.today
				@purchase.to = Date.today + 6.months
				current_user.organisation.purchases << @purchase
			else
				if current_user.free_content?
					current_purchase = current_user.purchases.where('purchases.price = ? AND purchases.to >= ?', 0.0, Date.today).first
					@purchase.from = current_purchase.to + 1.days
					@purchase.to = current_purchase.to + 1.days + 6.months
				else
					@purchase.from = Date.today + 1.days
					@purchase.to = Date.today + 6.months
				end

				current_user.purchases << @purchase
			end

			if @purchase.save
				flash[:success] = 'New purchase added successfully'
				if current_user.type == 'Organizer'
					redirect_to organisation_path(current_user.organisation)
				else
					redirect_to root_path
				end
			else
				flash[:error] = 'Validation error'
				render 'shop'
			end

		end

	rescue Stripe::CardError => e
		flash[:error] = e.message
		redirect_to new_purchase_path

	end

	private

	def purchase_params
		params.require(:purchase).permit(:price, :from, :to, chapter_ids: [])
	end

	def require_permission
		# Possède le rôle
		if current_user.type != 'Private'
			return redirect_to forbidden_path
		end

		# Appartient à l'utilisateur
		if params[:id].present?
			@purchase = Purchase.find(params[:id])
			if current_user != @purchase.purchasable
				return redirect_to forbidden_path
			end
		end
	end

end