class ManagersController < ApplicationController

  before_filter do |c|
    @organisation = Organisation.find(params[:organisation_id])
  end

  before_action :require_permission

  def show
    @manager = Manager.find(params[:id])
    @groups_grid = initialize_grid(Group.where('manager_id = ?', @manager.id))

    add_breadcrumb @manager.full_name, organisation_manager_path(@organisation,@manager), :title => "Back to the Index"
  end

  def new
    @manager = Manager.new
    add_breadcrumb "New manager", new_organisation_manager_path(@organisation), :title => "Back to the Index"
  end

  def edit
    @manager = Manager.find(params[:id])
    add_breadcrumb @manager.full_name, organisation_manager_path(@organisation,@manager), :title => "Back to the Index"
    add_breadcrumb "Edit", edit_organisation_manager_path(@organisation,@manager), :title => "Back to the Index"
  end

  def create
    @manager = Manager.new(manager_params)

    @organisation.managers << @manager
    
    if @manager.save
      flash[:success] = 'New manager added successfully'
      redirect_to organisation_manager_path(@organisation,@manager)
    else

      add_breadcrumb "New manager", new_organisation_manager_path(@organisation), :title => "Back to the Index"

      flash[:error] = 'Validation error'
      render 'new'
    end
  end

  def update
   @manager = Manager.find(params[:id])

    if @manager.update(manager_params)
      flash[:success] = 'Manger updated successfully'
      redirect_to organisation_manager_path(@organisation,@manager)
    else
      
      add_breadcrumb @manager.full_name, organisation_manager_path(@organisation,@manager), :title => "Back to the Index"
      add_breadcrumb "Edit", edit_organisation_manager_path(@organisation,@manager), :title => "Back to the Index"

      flash[:error] = 'Validation error'
      render 'edit'
    end
  end

  def destroy

    @manager = Manager.find(params[:id])

    if !@manager.destroy
      flash[:error] = @manager.errors[:base].first
    end

    respond_to do |format|
      format.html { redirect_to organisation_path(@organisation) }
      format.json { head :no_content }
    end
  end

private

  def manager_params
    params.require(:manager).permit(:first_name, :last_name, :email, :password, :password_confirmation)
  end

  def require_permission
    # Possède le rôle
    if current_user.type != 'Organizer'
      return redirect_to forbidden_path
    end

    # Manager appartient à la même organisation
    if params[:id].present?
      @manager = Manager.find(params[:id])
      if @manager.organisation != current_user.organisation
        return redirect_to forbidden_path
      end
    end

    # Apartient à l'organisation
    if params[:organisation_id].present?
      @organisation = Organisation.find(params[:organisation_id])
      if @organisation != current_user.organisation
        return redirect_to forbidden_path
      end
    end
  end


end