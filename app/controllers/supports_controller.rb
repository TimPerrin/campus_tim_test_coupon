class SupportsController < ApplicationController

  before_filter do |c|
    @chapter = Chapter.find(params[:chapter_id])
    @support = Support.find(params[:id])

    add_breadcrumb "My course", chapters_path, :title => "Back to the Index"
    add_breadcrumb @chapter.name, chapter_path(@chapter), :title => "Back to the Index"

    if params[:content_id].present?
      @content = Content.find(params[:content_id])
      add_breadcrumb @content.name, chapter_content_path(@chapter, @content, :support_category => @support.support_category.id), :title => "Back to the Index"
    end
  end

  before_action :require_permission


  def show

    @chapters = Chapter.get_all(current_user)
    chapter = Chapter.find(params[:chapter_id])
    @contents = Chapter.get_contents(current_user, chapter)
    @supports = Chapter.get_supports(current_user, chapter)

    # Get all the achievement for the students in my groups
    @achievements_grid = initialize_grid(Achievement
                                             .joins(:user)
                                             .where(support_id: @support.id)
                                             .where(:users => {:group_id => current_user.groups})) if current_user.type == 'Manager'
    # Encore utile ? Quentin 20.07.2017
    # -------------------------------- #

    # if params[:content_id].present?
    #   @supports = @content.supports.where(support_category: @support.support_category.id)
    #   @support_categories = SupportCategory.where.not(id: @support.support_category.id)
    #   add_breadcrumb @support.support_type.name, chapter_content_support_path(@chapter, @content, @support), :title => "Back to the Index"
    # else
    #   @supports = @chapter.supports.where(support_category: @support.support_category.id)
    #   add_breadcrumb @support.support_type.name, chapter_support_path(@chapter, @support), :title => "Back to the Index"
    # end

    if params[:content_id].present?
      @path = '/SUPPORT/' + @support.supportable.chapter.number.to_s + '/'
    else
      @path = '/SUPPORT/'
    end

    @path = @path + @support.supportable.number.to_s + '/' + @support.number.to_s

    # Handle achievement attribution
    case @support.support_type.name

      when 'Quiz'
        @achievement = Achievement.find_or_create_by(user_id: current_user.id, support_id: params[:id], score: 1)
        @achievement.ispring_token = SecureRandom.urlsafe_base64(nil)
        @achievement.save

      when 'Simulation'
        Achievement.find_or_create_by(user_id: current_user.id, support_id: params[:id], score: 50, ispring_token: SecureRandom.urlsafe_base64(nil, true))

      when 'Dictionary', 'Movie', 'Crossword', 'Word Search', 'AudioStory', 'Gap-fill', 'I Hume You'
        Achievement.find_or_create_by(user_id: current_user.id, support_id: params[:id], score: 100)

    end

    # Handle render attribution
    case @support.support_type.name

      when 'Simulation', 'Dictionary', 'Quiz'
        @path = @path + '/FILES/index.html'
        @render = 'ispring'

      when 'AudioStory'
        @story_path = @path + '/FILES/file.jpg'
        @path = @path + '/FILES/sound.mp3'
        @render = 'audio_story'

      when 'Gap-fill'
        @path = @path + '/FILES/index.html'
        @render = 'gap_fill'

      when 'I Hume You'
        @path = @path + '/FILES/index.html'
        @render = 'i_hume_you'

      when 'Movie'
        @path = @path + '/FILES/movie.mp4'
        @render = 'movie'

      when 'Crossword', 'Word Search'
        @path = @path + '/FILES/index.html'
        @render = 'crossword'

      when 'Print'
        @path = @path + '/FILES/file.pdf'
        @render = 'print'
    end

  end

  private

  def require_permission

    # Possède le chapitre
    if params[:chapter_id].present?
      chapter = Chapter.find(params[:chapter_id])
      chapters = Chapter.get_all(current_user)
      if !chapters.include?(chapter)
        return redirect_to forbidden_path
      end
    end

    # Possède le contenu
    if params[:content_id].present?
      content = Content.find(params[:content_id])
      chapter = Chapter.find(params[:chapter_id])
      contents = Chapter.get_contents(current_user, chapter)
      if !contents.include?(content)
        return redirect_to forbidden_path
      end
    end

    # Possède le support
    if params[:id].present?
      support = Support.find(params[:id])
      chapter = Chapter.find(params[:chapter_id])

      if support.supportable_type == "Content"
        contents = Chapter.get_contents(current_user, chapter)
        if !contents.include?(support.supportable)
          return redirect_to forbidden_path
        end
      else

        if !chapters.include?(support.supportable)
          return redirect_to forbidden_path
        end

        if current_user.type == 'Student'
          supports = Chapter.get_supports(current_user, chapter)
          if !supports.include?(support)
            return redirect_to forbidden_path
          end
        end

      end
    end

  end

end
