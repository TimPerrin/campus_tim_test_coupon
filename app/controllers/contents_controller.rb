class ContentsController < ApplicationController

  before_filter do |c|
    @chapter = Chapter.find(params[:chapter_id])
    add_breadcrumb "My course", chapters_path, :title => "Back to the Index"
    add_breadcrumb @chapter.name, chapter_path(@chapter), :title => "Back to the Index"
  end

  before_action :require_permission

  def show
    @content = Content.find(params[:id])
    @chapters = Chapter.get_all(current_user)
    @contents = Chapter.get_contents(current_user, @chapter)
    @supports = Chapter.get_supports(current_user, @chapter)

    add_breadcrumb @content.name, chapter_content_path(@chapter, @content), :title => "Back to the Index"
  end

  private

  def require_permission

    # Possède le chapitre
    if params[:chapter_id].present?
      chapter = Chapter.find(params[:chapter_id])
      chapters = Chapter.get_all(current_user)
      if !chapters.include?(chapter)
        return redirect_to forbidden_path
      end
    end

    # Possède le contenu
    if params[:id].present?
      content = Content.find(params[:id])
      chapter = Chapter.find(params[:chapter_id])
      contents = Chapter.get_contents(current_user, chapter)


      if !contents.include?(content)
        return redirect_to forbidden_path
      end
    end

  end

end