# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# Clear DB
Access.delete_all
Achievement.delete_all
Chapter.delete_all
ContentType.delete_all
Content.delete_all
Group.delete_all
Organisation.delete_all
Package.delete_all
Purchase.delete_all
Setting.delete_all
Subscription.delete_all
SupportCategory.delete_all
SupportType.delete_all
Support.delete_all
User.delete_all

# Organisations
organisation_1 = Organisation.create({name: 'Wavemind', address: 'Rue Galilee 15', zip: '1400', city: 'Yverdon-les-Bains', nb_account_student: '10', nb_account_manager: '2'})
organisation_2 = Organisation.create({name: 'Ducommun SA', address: 'Chemin de la Venoge 9', zip: '1024', city: 'Ecublens', nb_account_student: '20', nb_account_manager: '4'})
organisation_3 = Organisation.create({name: 'MEM SA', address: 'En Budron D5', zip: '1052', city: 'Mont-sur-Lausanne', nb_account_student: '30', nb_account_manager: '5'})
organisation_4 = Organisation.create({name: 'La Poste', address: 'Mingerstrasse 20', zip: '3030', city: 'Berne', nb_account_student: '40', nb_account_manager: '10'})
organisation_5 = Organisation.create({name: 'Merck Serono', address: 'Zone industriel', zip: '1267', city: ' Coinsins', nb_account_student: '67', nb_account_manager: '2'})
organisation_6 = Organisation.create({name: 'Etat de Vaud - SEO', address: 'Rue de la Barre 8', zip: '1014', city: 'Lausanne', nb_account_student: '24', nb_account_manager: '3'})
organisation_7 = Organisation.create({name: 'Etat de Neuchâtel - SEO', address: 'Rue de l\'Ecluse 67', zip: '2001', city: 'Neuchâtel', nb_account_student: '8', nb_account_manager: '3'})


# Managers
manager_1 = Manager.create({first_name: 'Jean', last_name: 'Bolomey', address: 'Avenue de la gare 4', zip: '1004', city: 'Lausanne', password: '12345678', password_confirmation: '12345678', email: 'jean.bolomey@vd.ch', organisation_id: organisation_6.id})
manager_2 = Manager.create({first_name: 'Bernard', last_name: 'Martignier', address: 'Rue central 15', zip: '1004', city: 'Lausanne', password: '12345678', password_confirmation: '12345678', email: 'bernard.martignier@vd.ch', organisation_id: organisation_3.id})
manager_3 = Manager.create({first_name: 'Daniel', last_name: 'Lacroix', address: 'Chemin du Levant 31', zip: '2001', city: 'Neuchâtel', password: '12345678', password_confirmation: '12345678', email: 'daniel.lacroix@wavemind.ch', organisation_id: organisation_1.id})

# Groups
group_1 = Group.create({name: 'SI-T2a', manager_id: manager_1.id})
group_2 = Group.create({name: 'MIN1', manager_id: manager_2.id})
group_3 = Group.create({name: 'CIN4b', manager_id: manager_3.id})

# Students
user_1 = Student.create({first_name: 'Michelle', last_name: 'Ischi', address: 'Chemin de l\'Esterelle', zip: '1315', city: 'La Sarraz', password: '12345678', password_confirmation: '12345678', email: 'michel.ischi@vd.ch', organisation_id: organisation_6.id, group_id: group_1.id})
user_2 = Student.create({first_name: 'David', last_name: 'Franco', address: 'Avenue de la gare 4', zip: '1004', city: 'Lausanne', password: '12345678', password_confirmation: '12345678', email: 'david.franco@vd.ch', organisation_id: organisation_6.id, group_id: group_1.id})
user_3 = Student.create({first_name: 'James', last_name: 'Bond', address: 'Avenue de la gare 4', zip: '1004', city: 'Lausanne', password: '12345678', password_confirmation: '12345678', email: 'james.bond@vd.ch', organisation_id: organisation_6.id, group_id: group_1.id})
user_4 = Student.create({first_name: 'Quentin', last_name: 'Girard', address: 'Chemin des Bosquets 12', zip: '1315', city: 'La Sarraz', password: '12345678', password_confirmation: '12345678', email: 'quentin.girard@hotmail.com', organisation_id: organisation_1.id, group_id: group_1.id})
user_5 = Student.create({first_name: 'Romain', last_name: 'Therisod', address: 'Rue de la Laiterie', zip: '1400', city: 'Yverdon-les-Bains', password: '12345678', password_confirmation: '12345678', email: 'romain.therisod@wavemind.ch', organisation_id: organisation_1.id, group_id: group_1.id})
user_6 = Student.create({first_name: 'Alain', last_name: 'Fresco', address: 'Avenue de la Gare 4', zip: '1004', city: 'Lausanne', password: '12345678', password_confirmation: '12345678', email: 'alain.fresco@wavemind.ch', organisation_id: organisation_1.id, group_id: group_3.id})



# Content types
verbs = ContentType.create({name: 'Verbs'})


# Support types
vocabulary = SupportType.create({name: 'Vocabulary', icon: 'fa-pencil-square-o'})
grammar = SupportType.create({name: 'Grammar', icon: 'fa-tags'})
stories = SupportType.create({name: 'Stories', icon: 'fa-quote-right'})
movies = SupportType.create({name: 'Movies', icon: 'fa-film'})
crosswords = SupportType.create({name: 'Crosswords', icon: 'fa-braille'})
games = SupportType.create({name: 'Games', icon: 'fa-puzzle-piece'})


# Contents
be = Content.create({name: 'To be', content_type_id: verbs.id})
blow = Content.create({name: 'To blow', content_type_id: verbs.id})
build = Content.create({name: 'To build', content_type_id: verbs.id})
choose = Content.create({name: 'To choose', content_type_id: verbs.id})
eat = Content.create({name: 'To eat', content_type_id: verbs.id})
have = Content.create({name: 'To have', content_type_id: verbs.id})
know = Content.create({name: 'To know', content_type_id: verbs.id})
sleep = Content.create({name: 'To sleep', content_type_id: verbs.id})


# Supports

## To Be
Support.create({name: 'toto', content_id: be.id, support_type_id: vocabulary.id })
Support.create({name: 'toto', content_id: be.id, support_type_id: grammar.id })
Support.create({name: 'toto', content_id: be.id, support_type_id: stories.id })
Support.create({name: 'toto', content_id: be.id, support_type_id: movies.id })
Support.create({name: 'toto', content_id: be.id, support_type_id: games.id })
Support.create({name: 'toto', content_id: be.id, support_type_id: crosswords.id })

## To Blow
Support.create({name: 'toto', content_id: blow.id, support_type_id: vocabulary.id })
Support.create({name: 'toto', content_id: blow.id, support_type_id: grammar.id })
Support.create({name: 'toto', content_id: blow.id, support_type_id: stories.id })
Support.create({name: 'toto', content_id: blow.id, support_type_id: movies.id })
Support.create({name: 'toto', content_id: blow.id, support_type_id: games.id })
Support.create({name: 'toto', content_id: blow.id, support_type_id: crosswords.id })

## To Build
Support.create({name: 'toto', content_id: build.id, support_type_id: vocabulary.id })
Support.create({name: 'toto', content_id: build.id, support_type_id: grammar.id })
Support.create({name: 'toto', content_id: build.id, support_type_id: stories.id })
Support.create({name: 'toto', content_id: build.id, support_type_id: movies.id })
Support.create({name: 'toto', content_id: build.id, support_type_id: games.id })
Support.create({name: 'toto', content_id: build.id, support_type_id: crosswords.id })

## To Choose
Support.create({name: 'toto', content_id: choose.id, support_type_id: vocabulary.id })
Support.create({name: 'toto', content_id: choose.id, support_type_id: grammar.id })
Support.create({name: 'toto', content_id: choose.id, support_type_id: stories.id })
Support.create({name: 'toto', content_id: choose.id, support_type_id: movies.id })
Support.create({name: 'toto', content_id: choose.id, support_type_id: games.id })
Support.create({name: 'toto', content_id: choose.id, support_type_id: crosswords.id })

## To Eat
Support.create({name: 'toto', content_id: eat.id, support_type_id: vocabulary.id })
Support.create({name: 'toto', content_id: eat.id, support_type_id: grammar.id })
Support.create({name: 'toto', content_id: eat.id, support_type_id: stories.id })
Support.create({name: 'toto', content_id: eat.id, support_type_id: movies.id })
Support.create({name: 'toto', content_id: eat.id, support_type_id: games.id })
Support.create({name: 'toto', content_id: eat.id, support_type_id: crosswords.id })

## To Have
Support.create({name: 'toto', content_id: have.id, support_type_id: vocabulary.id })
Support.create({name: 'toto', content_id: have.id, support_type_id: grammar.id })
Support.create({name: 'toto', content_id: have.id, support_type_id: stories.id })
Support.create({name: 'toto', content_id: have.id, support_type_id: movies.id })
Support.create({name: 'toto', content_id: have.id, support_type_id: games.id })
Support.create({name: 'toto', content_id: have.id, support_type_id: crosswords.id })

## To Know
Support.create({name: 'toto', content_id: know.id, support_type_id: vocabulary.id })
Support.create({name: 'toto', content_id: know.id, support_type_id: grammar.id })
Support.create({name: 'toto', content_id: know.id, support_type_id: stories.id })
Support.create({name: 'toto', content_id: know.id, support_type_id: movies.id })
Support.create({name: 'toto', content_id: know.id, support_type_id: games.id })
Support.create({name: 'toto', content_id: know.id, support_type_id: crosswords.id })

## To Sleep
Support.create({name: 'toto', content_id: sleep.id, support_type_id: vocabulary.id })
Support.create({name: 'toto', content_id: sleep.id, support_type_id: grammar.id })
Support.create({name: 'toto', content_id: sleep.id, support_type_id: stories.id })
Support.create({name: 'toto', content_id: sleep.id, support_type_id: movies.id })
Support.create({name: 'toto', content_id: sleep.id, support_type_id: games.id })
Support.create({name: 'toto', content_id: sleep.id, support_type_id: crosswords.id })


# Accesses
Access.create({active: 1, group_id: group_1.id,content_id: be.id})
Access.create({active: 1, group_id: group_1.id,content_id: blow.id})
Access.create({active: 1, group_id: group_1.id,content_id: build.id})
Access.create({active: 1, group_id: group_1.id,content_id: choose.id})
Access.create({active: 1, group_id: group_1.id,content_id: eat.id})
Access.create({active: 1, group_id: group_1.id,content_id: have.id})
Access.create({active: 1, group_id: group_1.id,content_id: know.id})
Access.create({active: 1, group_id: group_1.id,content_id: sleep.id})

Access.create({active: 1, group_id: group_2.id,content_id: be.id})
Access.create({active: 1, group_id: group_2.id,content_id: blow.id})
Access.create({active: 1, group_id: group_2.id,content_id: build.id})
Access.create({active: 1, group_id: group_2.id,content_id: choose.id})
Access.create({active: 1, group_id: group_2.id,content_id: eat.id})
Access.create({active: 1, group_id: group_2.id,content_id: have.id})
Access.create({active: 1, group_id: group_2.id,content_id: know.id})
Access.create({active: 1, group_id: group_2.id,content_id: sleep.id})

