class AddNumberToContentsAndSupports < ActiveRecord::Migration
  def change

  	add_column :contents, :number, :integer
  	add_column :supports, :number, :integer

  end
end
