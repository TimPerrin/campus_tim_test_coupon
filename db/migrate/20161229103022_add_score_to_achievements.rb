class AddScoreToAchievements < ActiveRecord::Migration
  def change
    add_column :achievements, :score, :float, :default => 0.0
  end
end
