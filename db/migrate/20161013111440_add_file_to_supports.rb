class AddFileToSupports < ActiveRecord::Migration
  def change
    add_column :supports, :file, :string
  end
end
