class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name

      t.belongs_to :manager

      t.timestamps
    end

    add_column :accesses, :group_id, :integer
    add_column :users, :group_id, :integer
    remove_column :accesses, :user_id
    remove_column :users, :manager_id
  end
end
