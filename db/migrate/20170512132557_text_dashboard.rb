class TextDashboard < ActiveRecord::Migration
  def change
  	add_column :groups, :description, :text
  	add_column :users, :description, :text

    create_table :settings do |t|
    	t.text :description
    end

  end
end
