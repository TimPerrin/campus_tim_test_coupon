class CreateAccess < ActiveRecord::Migration
  def change
    create_table :accesses do |t|
      t.boolean :active

      t.belongs_to :user
      t.belongs_to :content

      t.timestamps
    end

    add_index :accesses, :user_id
    add_index :accesses, :content_id
  end
end
