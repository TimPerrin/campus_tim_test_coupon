class AddSubscriptionsSystem < ActiveRecord::Migration
  def change

    create_table :purchases do |t|
    	t.belongs_to :purchasable, :polymorphic => true
    	t.datetime :from
    	t.datetime :to
    	t.float :price

    	t.timestamps
    end

    create_table :subscriptions do |t|
    	t.belongs_to :purchase
    	t.belongs_to :chapter
    end

  end
end
