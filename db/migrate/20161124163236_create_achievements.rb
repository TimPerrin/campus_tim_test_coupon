class CreateAchievements < ActiveRecord::Migration
  def change
    create_table :achievements, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :support, index: true

      t.timestamps
    end
  end
end
