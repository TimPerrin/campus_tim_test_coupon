class EditPriceToPurchase < ActiveRecord::Migration
  def change
    change_column :purchases, :price, :decimal, :precision => 4, :scale => 2
  end
end
