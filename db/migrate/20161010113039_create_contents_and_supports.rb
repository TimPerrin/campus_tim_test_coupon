class CreateContentsAndSupports < ActiveRecord::Migration
  def change
    create_table :contents do |t|
    	t.string :name
    	t.belongs_to :content_type
    	t.timestamps
    end

    create_table :content_types do |t|
    	t.string :name
    	t.timestamps
    end 

    create_table :supports do |t|
    	t.string :name
    	t.belongs_to :content
    	t.belongs_to :support_type
    	t.timestamps
    end

    add_index :supports, :content_id

    create_table :support_types do |t|
    	t.string :name
    	t.integer :priority
    	t.timestamps
    end 

    create_table :packages do |t|
    	t.belongs_to :content_type
    	t.belongs_to :support_type
    	t.timestamps
    end   

  end
end
