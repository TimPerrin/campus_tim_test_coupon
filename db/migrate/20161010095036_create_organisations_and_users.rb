class CreateOrganisationsAndUsers < ActiveRecord::Migration
  def change
    create_table :organisations do |t|
      t.string :name
      t.string :address
      t.string :zip
      t.string :city
      t.integer :nb_account_student
      t.integer :nb_account_manager

      t.timestamps
    end

    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :zip
      t.string :city
      t.string :type
      t.belongs_to :organisation

      t.timestamps
    end

    add_index :users, :organisation_id
  end
end
