class AddStripeInfoToPurchase < ActiveRecord::Migration
  def change
    add_column :purchases, :stripe_id, :string
    add_column :purchases, :stripe_card, :string
    add_column :purchases, :stripe_last4, :integer
  end
end
