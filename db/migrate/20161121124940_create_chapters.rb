class CreateChapters < ActiveRecord::Migration
  def change

    create_table :chapters do |t|
    	t.string :name
    	t.integer :number
    	t.text :description
        t.string :thumbnail

    	t.timestamps
    end

    add_column :contents, :chapter_id, :integer

    # Polymorphique pour les accès
	remove_column :accesses, :content_id
	add_column :accesses, :accessable_id, :integer
	add_column :accesses, :accessable_type, :string

	#remove_index :accesses, :name => 'index_accesses_on_content_id'

	# Polymorphique pour les supports
	remove_column :supports, :content_id
	add_column :supports, :supportable_id, :integer
	add_column :supports, :supportable_type, :string

	#remove_index :supports, :name => 'index_supports_on_content_id'

  end
end
