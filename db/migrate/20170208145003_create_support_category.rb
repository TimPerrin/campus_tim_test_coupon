class CreateSupportCategory < ActiveRecord::Migration
  def change
    create_table :support_categories do |t|
    	t.string :name
    	t.text :description
    end

    add_column :supports, :support_category_id, :integer
  end
end
