class AddTokenToAchievement < ActiveRecord::Migration
  def change
    add_column :achievements, :ispring_token, :string
    add_column :achievements, :id, :primary_key
  end
end
